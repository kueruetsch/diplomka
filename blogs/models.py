from django.db import models


class Blog(models.Model):
    title = models.CharField(max_length=125)
    description = models.TextField(null=True, blank=True)
    content = models.TextField(null=True, blank=True)

    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        ordering = ('title',)
