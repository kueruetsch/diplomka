from django.db import models


class Teacher(models.Model):
    name = models.CharField(max_length=125)
    title = models.CharField(max_length=125, null=True, blank=True)

    description = models.TextField(null=True, blank=True)

    image = models.ImageField(null=True, blank=True)
    icon = models.ImageField(null=True, blank=True)

    email = models.EmailField(null=True, blank=True)
    phone_number = models.CharField(max_length=125, null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        ordering = ('name',)
