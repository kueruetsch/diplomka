from rest_framework.viewsets import ModelViewSet

from faculty.models import Teacher
from faculty.serializers import TeacherSerializer


class FacultyViewSet(ModelViewSet):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
