# Diplomka

    pip install -r requirements.txt
    python manage.py migrate
    python manage.py createsuperuser
    python manage.py runserver
    

## API endpoints

### Blogs
#### `GET api/v1/blogs`
Get list of blogs

#### `GET api/v1/blogs/{id}`
Get specific blog

### Teachers
#### `GET api/v1/faculty`
Get list of teachers

#### `GET api/v1/faculty/{id}`
Get specific teacher